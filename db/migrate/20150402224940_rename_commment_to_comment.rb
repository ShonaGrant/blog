class RenameCommmentToComment < ActiveRecord::Migration
  def self.up
    rename_column :comments, :commment, :comment
  end

  def self.down
    rename_column :comments, :commment, :comment
  end
end